#!/usr/bin/env python3

import configparser
import git
import gitlab
import glob
import hashlib
import os
import re
import requests
import shutil
import subprocess
import sys

from urllib.parse import parse_qs, urlparse

JAVA_PACKAGENAME = '([a-zA-Z_]{1}[a-zA-Z]*){2,10}\.([a-zA-Z_]{1}[a-zA-Z0-9_]*){1,30}((\.([a-zA-Z_]{1}[a-zA-Z0-9_]*){1,61})*)?'
GPLAY_PATTERN = re.compile('http[s]?://play\.google\.com/store/apps/details\?id=' + JAVA_PACKAGENAME)
GIT_PATTERN = re.compile('http[s]?://(github.com|gitlab.com|bitbucket.org|git.code.sf.net)/[\w.-]+/[\w.-]+')
PACKAGE_ID_PATTERN = re.compile('PACKAGE ?ID:?\s*' + JAVA_PACKAGENAME, re.IGNORECASE | re.DOTALL)
# find all repositories that use plain HTTP urls (e.g. not HTTPS)
HTTP_GRADLE_PATTERN = re.compile('repositories\s*{[^}]*http://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+[^}]*}', re.DOTALL)

PERSONAL_ACCESS_TOKEN = os.getenv('PERSONAL_ACCESS_TOKEN')
VIRUSTOTAL_API_KEY = os.getenv('VIRUSTOTAL_API_KEY')
gl = gitlab.Gitlab('https://gitlab.com', api_version=4,
                   private_token=PERSONAL_ACCESS_TOKEN)
rfp = gl.projects.get('eighthave/rfp-test')

os.makedirs('build', exist_ok=True)
os.makedirs('metadata', exist_ok=True)
os.makedirs('repo', exist_ok=True)
with open('fdroid-icon.png', 'w') as fp:
    fp.write('')
with open('config.py', 'w') as fp:
    fp.write('repo_pubkey = "deadbeef"\n')
    fp.write('androidobservatory = True\n')
    if VIRUSTOTAL_API_KEY:
        fp.write('virustotal_apikey = "%s"\n' % VIRUSTOTAL_APIKEY)

for issue in rfp.issues.list(state='opened', all=True):
    note = ''
    security = ''
    labels = set(issue.labels)
    labels.add('fdroid-bot')

    print('Checking', issue.title)

    appid = None
    gplay_urls = set()
    for m in GPLAY_PATTERN.finditer(issue.description):
        gplay_urls.add(m.group(0))
    for url in sorted(gplay_urls):
        appid = parse_qs(urlparse(url).query)['id'][0]
        cmd = ['gplaycli', '-d', appid]
        print('$ %s' % ' '.join(cmd))
        sp = subprocess.run(cmd, stderr=subprocess.STDOUT)
        print(sp.stdout)

    if not appid:
        m = PACKAGE_ID_PATTERN.search(issue.description)
        if m:
            appid = m.group(0).split(':')[1].strip()

    if appid:
        for filetype in ('.txt', '.yml'):
            fdroiddata_url = 'https://gitlab.com/fdroid/fdroiddata/blob/master/metadata/' + appid + filetype
            r = requests.head(fdroiddata_url)
            if r.status_code == 200:
                note += 'Closing issue, %s already in fdroiddata:\n%s\n\n' % (appid, fdroiddata_url)
                note += '* found %s\n' %url
                labels.add('in-fdroiddata')
                issue.state_event = 'close'
        gplay_urls.add('https://play.google.com/store/apps/details?id=' + appid)
        urls = set(gplay_urls)
        urls.add('https://apt.izzysoft.de/fdroid/index/apk/' + appid)
        for url in urls:
            r = requests.head(url)
            if r.status_code == 200:
                note += '* found %s\n' %url
                if url.startswith('https://play.google.com'):
                    labels.add('in-google-play')
                elif url.startswith('https://apt.izzysoft.de'):
                    labels.add('in-izzysoft')

    repo = None
    git_urls = set()
    for m in GIT_PATTERN.finditer(issue.description):
        url = m.group(0)
        if not url.startswith('https://gitlab.com/fdroid/'):
            git_urls.add(url)
    for url in sorted(git_urls):
        r = requests.head(url)
        if r.status_code == 200:
            if appid:
                with open(os.path.join('metadata', appid + '.yml'), 'w') as fp:
                    fp.write('RepoType: git\nRepo: %s\n' % url)
                repo_path = os.path.join('build', appid)
                print('$ git clone', url, repo_path)
                repo = git.Repo.clone_from(url, repo_path)
                cmd = ['fdroid', 'scanner', '--verbose', appid]
                print('$ %s' % ' '.join(cmd))
                scanner_output = subprocess.check_output(cmd, stderr=subprocess.STDOUT).decode()
                if 'WARNING:' in scanner_output:
                    labels.add('scanner-warning')
                if 'ERROR:' in scanner_output:
                    labels.add('scanner-error')
                note += '\n\n## fdroid scanner\n\n'
                note += '%s:\n\n```console\n$ %s\n' % (url, ' '.join(cmd))
                note += scanner_output + '\n```\n\n'

                insecure_repositories = set()
                for root, dirs, files in os.walk(repo_path):
                    for f in files:
                        if f.endswith('.gradle'):
                            labels.add('gradle')
                            path = os.path.join(root, f)
                            with open(path) as fp:
                                data = fp.read()
                            for url in HTTP_GRADLE_PATTERN.findall(data):
                                print('Found plain HTTP URL for gradle repository:\n%s\n%s'
                                      % (path, url))
                                insecure_repositories.add(url)
                if insecure_repositories:
                    security += ('* gradle build uses %d plain HTTP URLs for repositories!  This is insecure! See: %s\n'
                                 % (len(insecure_repositories),
                                    'https://max.computer/blog/how-to-take-over-the-computer-of-any-java-or-clojure-or-scala-developer/'))
                    for url in insecure_repositories:
                        security += '  * %s\n' % url
                    labels.add('insecure-repositories')

                prop = os.path.join(repo_path, 'gradle', 'wrapper', 'gradle-wrapper.properties')
                if os.path.exists(prop):
                    labels.add('gradle')
                    with open(prop) as fp:
                        propdata = fp.read()
                    config = configparser.ConfigParser()
                    config.read_string('[DEFAULT]\n' + propdata)  # fake a INI file
                    url = config['DEFAULT']['distributionUrl'].replace('\\', '')
                    if url.startswith('http:'):
                        labels.add('insecure-gradlew')
                        security += '* Insecure HTTP gradle download, use _%s_!\n' \
                                    % url.replace('http:', 'https:')
                    if 'distributionSha256Sum' not in config['DEFAULT']:
                        labels.add('insecure-gradlew')
                        security += ('* _gradle/wrapper/gradle.properties_ is missing [distributionSha256Sum]('
                                     + 'https://docs.gradle.org/current/userguide/gradle_wrapper.html#sec:verification), '
                                     + 'unverified gradle download!')
                        # the stream=True parameter keeps memory usage low
                        r = requests.get(url, stream=True, allow_redirects=True)
                        if r.status_code == 200:
                            hasher = hashlib.sha256()
                            gradle = os.path.basename(urlparse(url).path)
                            print('Downloading', url, 'to', gradle)
                            with open(gradle, 'wb') as fp:
                                for chunk in r.iter_content(chunk_size=64*1024):
                                    if chunk:  # filter out keep-alive new chunks
                                        fp.write(chunk)
                                        hasher.update(chunk)
                                fp.flush()
                            security += (' Example:\n\n'
                                         + '```properties\n{propdata}\ndistributionSha256Sum={sha256}\n```\n\n'
                                         .format(propdata=propdata.strip(), sha256=hasher.hexdigest()))
                        else:
                            security += '\n\n'
                break

    report = ''
    if repo:
        for tag in repo.tags:
            suspicious = set()
            for c in re.findall(r'[^a-zA-Z0-9.,()/+_#@-]', tag.name):
                suspicious.add(c)
            if suspicious:
                report += "* %s (%s)\n" % (tag, ''.join(sorted(suspicious)))
        if report:
            report = '\n\n### Suspicious tag/branch names\n\n' + report
    if security or report:
        note += '\n\n## Security Issues\n\n' + security + '\n\n' + report

    scanners = ''
    if appid:
        scanners += '* [APK Scan %s](https://apkscan.org/?searchby=pkg&q=%s)\n' \
                    % (appid, appid)
        apk = appid + '.apk'
        if os.path.exists(apk):
            hasher = hashlib.sha256()
            with open(apk, 'rb') as fp:
                while True:
                    chunk = fp.read(65536)
                    if not chunk:
                        break
                    hasher.update(chunk)
            sha256 = hasher.hexdigest()
            scanners += '* [APK Scan](https://apkscan.org/?searchby=hash&q=%s)\n' % sha256
            vturl = 'https://www.virustotal.com/file/' + sha256
            r = requests.head(vturl)
            if r.status_code == 200:
                labels.add('virustotal')
                scanners += '* [VirusTotal](%s)\n' % vturl

        t = requests.get('https://reports.exodus-privacy.eu.org/api/trackers')
        r = requests.get('https://reports.exodus-privacy.eu.org/api/search/%s' % appid)
        if t.status_code == 200 and r.status_code == 200:
            try:
                trackers = t.json()['trackers']
                reports = r.json()
                if appid in reports:
                    exodus = ''
                    reports = r.json()[appid]['reports']
                    if len(reports) > 0:
                        for trackernum in reports[0]['trackers']:
                            tracker = trackers[str(trackernum)]
                            exodus += '* [%s](%s)\n' % (tracker['name'], tracker['website'])
                    if exodus:
                        labels.add('trackers')
                        scanners += '### [Exodus Privacy](https://reports.exodus-privacy.eu.org/search)\n\n'
                        scanners += exodus
            except Exception as e:
                print(appid, '-', e)

    if scanners:
        note += '\n\n## External Scanners\n\n' + scanners + '\n\n'

    emojis = issue.awardemojis.list()
    add_robot = True
    for emoji in emojis:
        if emoji.name == 'robot':
            add_robot = False
            break
    if add_robot:
        issue.awardemojis.create({'name': 'robot'})

    if appid:
        labels.add(appid)
    if labels:
        issue.labels = sorted(labels)

    if note:
        n = issue.notes.create({'body': note})
    issue.save()

# attempt to upload all APKs to APK Scan
cmd = 'fdroid update --create-metadata --nosign'
print('$ ' + cmd)
for f in glob.glob('*.apk'):
    shutil.move(f, 'repo')
subprocess.call(cmd.split())

cmd = 'fdroid server update --verbose'
print('$ ' + cmd)
subprocess.call(cmd.split())
